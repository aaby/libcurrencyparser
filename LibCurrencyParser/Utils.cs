﻿/* LibCurrencyParser is released under the MIT license
 * 
 * Copyright 2019 Arno Aby <aaby@disroot.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace LibCurrencyParser
{
    public static class Utils
    {
        public static bool IsAllowed(this CurrencyParsingOptions o, char c)
        {
            return IsDigit(c)
                || o.IsSeparator(c);
        }

        public static string TrimInvalidChars(this CurrencyParsingOptions o, string s)
        {
            int _ = 0;
            return TrimInvalidChars(o, s, ref _);
        }

        internal static string TrimInvalidChars(this CurrencyParsingOptions o, string s, ref int caretpos)
        {
            if (s == null)
                return s;

            var sb = new StringBuilder();
            for (int i = 0; i < s.Length; i++)
            {
                var c = s[i];
                if (o.IsAllowed(c))
                    sb.Append(c);
                else if (i < caretpos)
                    caretpos--;
            }
            return sb.ToString();
        }

        public static bool IsDigit(char c)
        {
            return c >= '0' && c <= '9';
        }

        public static bool IsSeparator(this CurrencyParsingOptions options, char c)
        {
            var nf = options.Culture.NumberFormat;

            if (nf.CurrencyDecimalSeparator.Contains(c))
                return true;

            if (options.AllowDigitGroupSeparator &&
                nf.CurrencyGroupSeparator.Contains(c))
                return true;

            return false;
        }

        public static bool Contains(this string s, char c)
        {
            for (int i = 0; i < s.Length; i++)
                if (s[i] == c)
                    return true;
            return false;
        }
    }
}
