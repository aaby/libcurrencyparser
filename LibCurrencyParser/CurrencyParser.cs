﻿/* LibCurrencyParser is released under the MIT license
 * 
 * Copyright 2019 Arno Aby <aaby@disroot.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

using System.Globalization;

namespace LibCurrencyParser
{
    public class CurrencyParser
    {
        private CurrencyParsingOptions _options;
        public CurrencyParsingOptions Options
        {
            get
            {
                if (_options == null)
                    _options = CurrencyParsingOptions.Default;
                return _options;
            }
            set
            {
                if (value == null)
                    value = CurrencyParsingOptions.Default;
                _options = value;
            }
        }

        public ParsingResult Parse(
            string text,
            int caretPosition)
        {
            var nf = Options.Culture.NumberFormat;
            
            int curpos = caretPosition;

            if (Options.TrimInvalidChars)
                text = Options.TrimInvalidChars(text, ref curpos);

            var x = CurrencyValue.Parse(text, Options);
            if (x == null)
                return new ParsingResult(false, text, caretPosition, curpos);

            int leftTrimmed = 0;

            if (!Options.AllowLeadingIntegralZero)
            {
                while (
                    (text.Length > 1 && text[0] == '0')
                    || text.StartsWith(nf.CurrencyGroupSeparator))
                {
                    var n = text.Substring(1);
                    if (n.StartsWith(nf.CurrencyDecimalSeparator))
                        break;

                    text = n;
                    leftTrimmed++;
                }
                curpos -= leftTrimmed;
            }

            var fd = Options.FractionalDigits;

            if (x.RawFractionalPart.Length > fd)
            {
                text = x.TruncateDecimals(fd)
                    .Formatted;
                curpos = text.Length;
            }

            if (curpos < 0)
                curpos = 0;

            if (Options.ValidateRightmostGroup
                && !ValidateLastGrouping(text, nf))
                return new ParsingResult(false, text, caretPosition, curpos);

            if (Options.ApplyCurrencySymbol != null)
                text += $"{Options.ApplyCurrencySymbol}";

            return new ParsingResult(
                true,
                text,
                caretPosition,
                curpos,
                x);
        }

        private bool ValidateLastGrouping(string text, NumberFormatInfo nf)
        {
            if (nf.CurrencyGroupSizes.Length == 0
                || string.IsNullOrEmpty(nf.CurrencyGroupSeparator))
                return true;

            var startidx = text.LastIndexOf(nf.CurrencyGroupSeparator);
            if (startidx < 0)
                return true;

            if (startidx + nf.CurrencyGroupSeparator.Length == text.Length)
                return false; //group separator is the last portion of text

            var lastidx =
                string.IsNullOrEmpty(nf.CurrencyDecimalSeparator)
                ? text.Length
                : text.IndexOf(nf.CurrencyDecimalSeparator, startidx + 1);

            if (lastidx < 0)
                lastidx = text.Length;
            else if (lastidx < startidx)
                return false;

            return lastidx -1 - startidx == nf.CurrencyGroupSizes[0];
        }
    }

    public class ParsingResult
    {
        public ParsingResult(
            bool isValid,
            string formattedResult = null,
            int oldCaretPosition = -1,
            int newCaretPosition = -1,
            CurrencyValue value = null)
        {
            IsValid = isValid;
            FormattedResult = formattedResult;
            OldCaretPosition = oldCaretPosition;
            NewCaretPosition = newCaretPosition;
            CurrencyValue = value;
        }

        public bool IsValid { get; }
        public string FormattedResult { get; }
        public CurrencyValue CurrencyValue { get; }
        public int OldCaretPosition { get; }
        public int NewCaretPosition { get; }

    }

}
