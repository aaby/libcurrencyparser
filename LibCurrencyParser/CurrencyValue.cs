﻿/* LibCurrencyParser is released under the MIT license
 * 
 * Copyright 2019 Arno Aby <aaby@disroot.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

using System.Globalization;

namespace LibCurrencyParser
{
    public class CurrencyValue
    {
        public static CurrencyValue Zero { get; }
            = new CurrencyValue("0", 0, CurrencyParsingOptions.Default);

        public string Currency
        {
            get
            {
                return Formatted + $"{Options.ApplyCurrencySymbol}";
            }
        }
        public string Formatted
            => Numeric.ToString($"N{DecimalPlaces}", Nf);

        private decimal _numeric;
        public decimal Numeric
        {
            get => _numeric;
            set
            {
                _numeric = value;
                RawText = Formatted;
            }
        }

        public CultureInfo Culture
            => Options.Culture;

        public int DecimalPlaces { get; }

        public string RawText { get; private set; }

        public string RawFractionalPart
        {
            get
            {
                var sepidx = RawText.LastIndexOf(Culture.NumberFormat.CurrencyDecimalSeparator);

                if (sepidx < 0 || sepidx == RawText.Length - 1)
                    return string.Empty;
                else
                    return RawText.Substring(sepidx + 1);
            }
        }

        private NumberFormatInfo Nf
            => Options.AllowDigitGroupSeparator
            ? Culture.NumberFormat
            : CustomNumFmt;

        private NumberFormatInfo CustomNumFmt
        {
            get
            {
                var _nf = (NumberFormatInfo) Options.Culture.NumberFormat.Clone();
                if (!Options.AllowDigitGroupSeparator)
                {
                    _nf.CurrencyGroupSeparator = "";
                    _nf.NumberGroupSeparator = "";
                }
                return _nf;
            }
        }

        private CurrencyParsingOptions Options { get; }

        private CurrencyValue(
            string rawText,
            decimal numeric,
            CurrencyParsingOptions options)
        {
            RawText = rawText;
            Options = options;

            if (Options.FractionalDigits > 0)
                DecimalPlaces = options.FractionalDigits;
            else
                DecimalPlaces = 0;

            _numeric = numeric;
        }

        public CurrencyValue TruncateDecimals(int decimalDigits)
        {
            if (decimalDigits < 0)
                decimalDigits = 0;

            var mystr = Numeric.ToString("F99", Nf);
            var sepidx = mystr.LastIndexOf(Nf.CurrencyDecimalSeparator);

            if (sepidx >= 0)
            {
                if (decimalDigits == 0)
                    mystr = mystr.Substring(0, sepidx);
                else
                {
                    var maxlen = sepidx + 1 + decimalDigits;
                    if (mystr.Length < maxlen)
                        maxlen = mystr.Length;
                    mystr = mystr.Substring(0, maxlen);
                }
            }
            return Parse(mystr, Options);
        }

        public static CurrencyValue Parse(
            string text,
            CurrencyParsingOptions options = null)
        {
            if (options == null)
                options = CurrencyParsingOptions.Default;

            var nsty = NumberStyles.AllowDecimalPoint;
            if (options.AllowDigitGroupSeparator)
                nsty |= NumberStyles.AllowThousands;
            if (options.AllowSign)
                nsty |= NumberStyles.AllowLeadingSign;

            if (options.ApplyCurrencySymbol != null)
                text = text
                    .Replace(options.ApplyCurrencySymbol, string.Empty);

            decimal currency;
            if (!decimal.TryParse(text,
                nsty,
                options.Culture,
                out currency))
            {
                return null;
            }

            return new CurrencyValue(text, currency, options);
        }

    }

}
