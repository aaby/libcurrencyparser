﻿/* LibCurrencyParser is released under the MIT license
 * 
 * Copyright 2019 Arno Aby <aaby@disroot.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the “Software”),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

using System.Globalization;

namespace LibCurrencyParser
{
    public class CurrencyParsingOptions
    {
        public static CurrencyParsingOptions Default { get; }
            = new CurrencyParsingOptions()
            {
                AllowDigitGroupSeparator = true,
                AllowSign = true,
                AllowLeadingIntegralZero = false,
                FractionalDigits = 2,
                Culture = CultureInfo.InvariantCulture,
                ApplyCurrencySymbol = null,
                TrimInvalidChars  = true
            };

        public bool AllowDigitGroupSeparator { get; set; } = true;
        public bool AllowSign { get; set; } = true;
        public bool AllowLeadingIntegralZero { get; set; } = true;
        public bool ValidateRightmostGroup { get; set; } = true;
        public bool TrimInvalidChars { get; set; } = true;

        private CultureInfo _culture;
        public CultureInfo Culture
        {
            get
            {
                if (_culture == null)
                    _culture = CultureInfo.CurrentCulture;
                return _culture;
            }
            set
            {
                if (value == null)
                    value = CultureInfo.CurrentCulture;
                _culture = value;
            }
        }

        private int? _fractionalDigits;
        public int FractionalDigits
        {
            get
            {
                if (!_fractionalDigits.HasValue)
                    _fractionalDigits = Culture.NumberFormat.CurrencyDecimalDigits;
                return _fractionalDigits.Value;
            }
            set
            {
                _fractionalDigits = value;
            }
        }
        public string ApplyCurrencySymbol { get; set; }
    }

}
